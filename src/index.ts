import * as $ from 'jquery';

let url = 'http://localhost:8080';
//On récupère le token dans le sessionStorage
let token = sessionStorage.getItem('token');
//Si le token est présent on affiche certains trucs et on cache
//le formulaire de login.
if(token) {
    $('#logged').show();
    $('#login').hide();
    //Si on a le token, on fait une requête vers notre api en
    //fournissant celui ci dans le header Authorization Bearer
    $.ajax(url+'/shopping-list',
    {
        headers: {
            'Authorization': 'Bearer '+token
        }
    })
    .then(resp => {
        
        resp = JSON.parse(resp);
        for(let item of resp) {
            console.log(item);
            
            let el = document.createElement('article');
            $(el).text(item.name).appendTo('#logged');
        }
    }).catch(err => {
        /**
         * On gère le catch pour le cas où le token serait expiré
         */
        if(err) {
            $('#logged').hide();
            $('#login').show();
        }
    });

}

/**
 * On gère le submit du formulaire de login.
 */
$('#login').on('submit', event => {
    event.preventDefault();
    //On fait une requête ajax vers la route de login de notre api
    $.ajax(url+'/login_check',
    {
        method: 'POST',
        //On passe username/password en json en data
        data: JSON.stringify({
            username: $('#username').val(), 
            password: $('#password').val()
        }),
        contentType:'application/json' 
    })
    .then( resp => {
        //Si le login fonctionne, on stock le token dans le sessionStorage
        sessionStorage.setItem('token', resp.token);
        $('#logged').show();
        $('#login').hide();
    })
    //Sinon il serait de bon ton d'afficher une ptite erreur
    .catch(err => console.error(err));

});
/**
 * Au click sur le logout, on supprime le token du sessionStorage
 */
$('#logout').on('click', () => {
    sessionStorage.removeItem('token');
    $('#logged').hide();
        $('#login').show();

});